#. extracted from ../remmina_news.1.3.4.md
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-17 17:02+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 2.3.1\n"

#: ../remmina_news.1.3.4.md:2
msgid "<big>What's new in Remmina <b>1.3.4</b>.</big>"
msgstr ""

#: ../remmina_news.1.3.4.md:14
msgid ""
"<tt>\n"
"* <b>WWW (web browser) plugin preview.</b>;\n"
"* remmina://, rdp://, vnc:// and spice:// protocol handlers;\n"
"* News widget;\n"
"* Custom profile file names;\n"
"* Master password;\n"
"* SSH authentication bugs;\n"
"* Snap package update;\n"
"* Flatpak package update;\n"
"* Many bug fixing.\n"
"</tt>"
msgstr ""

#: ../remmina_news.1.3.4.md:18
msgid ""
"<span>\n"
"The <b>WWW plugin preview</b> it's at an early developemet stage, feel free "
"to contact us on IRC or send bug report on GitLab.\n"
"</span>"
msgstr ""

#: ../remmina_news.1.3.4.md:21
msgid ""
"<span>\n"
"More details can be found in the <a href=\"https://gitlab.com/Remmina/"
"Remmina/blob/master/CHANGELOG.md\" title=\"<i>v1.3.4</i> full changelog\""
">changelog.</a>\n"
"</span>"
msgstr ""
