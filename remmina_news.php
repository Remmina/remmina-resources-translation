<?php
$language = $_GET['lang'];
$version = $_GET['ver'];

$fileName = 'remmina_news.'.$language.'.'.$version.'.md';
if ( file_exists($fileName) == false ) {
    $fileName = 'remmina_news.en_US.'.$version.'.md';
}
if ( file_exists($fileName) && ($fp = fopen($fileName, "r"))!==false ) {

    fpassthru($fp);
}
else
{
    http_response_code(404);
}

?>
