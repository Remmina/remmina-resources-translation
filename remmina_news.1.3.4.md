<big>What's new in Remmina <b>1.3.4</b>.</big>

<span>
<b>Remmina v1.3.5 Ice Pop Edition has just been released</b>
See  <a href="https://remmina.org/how-to-install-remmina/">How to install Remmina</a> for help on how to upgrade.
</span>

<tt>
* <b>WWW (web browser) plugin preview.</b>;
* remmina://, rdp://, vnc:// and spice:// protocol handlers;
* News widget;
* Custom profile file names;
* Master password;
* SSH authentication bugs;
* Snap package update;
* Flatpak package update;
* Many bug fixing.
</tt>

<span>
The <b>WWW plugin preview</b> it's at an early developemet stage, feel free to contact us on IRC or send bug report on GitLab.
</span>

<span>
More details can be found in the <a href="https://gitlab.com/Remmina/Remmina/blob/master/CHANGELOG.md" title="<i>v1.3.4</i> full changelog">changelog.</a>
</span>
