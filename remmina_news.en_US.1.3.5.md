<big>What's new in Remmina <b>1.3.5</b>.</big>

<tt>
* <b>WWW (web browser) plugin preview.</b>;
* Custom profile file names;
* KWallet secret plugin;
* News widget;
* Remmina Connection Window refactoring;
* SSH authentication bugs;
* remmina://, rdp://, vnc:// and spice:// protocol handlers;
* Flatpak package update;
* Many bug fixing.
* Many new strings translated.
* Master password;
* Snap package update;
</tt>

<span>
The <b>WWW plugin preview</b> it's at an early development stage, feel free to contact us on IRC or send bug report on GitLab.
</span>

<span>
<b>Passwords cannot be automatically exported from one keystore to to other (Kwallet, Gnome Keyring).</b>
</span>

<span>
More details can be found in the <a href="https://gitlab.com/Remmina/Remmina/blob/master/CHANGELOG.md" title="<i>v1.3.5</i> full changelog">changelog.</a>.
</span>

